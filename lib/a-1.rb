# Given an array of unique integers ordered from least to greatest, write a
# method that returns an array of the integers that are needed to
# fill in the consecutive set.

def missing_numbers2(nums)
  arr = (nums.min..nums.max).to_a
  result = arr.dup
  arr.each do |n|
    result.delete(n) if nums.include?(n)
  end
  result
end

def missing_numbers(nums)
  arr = (nums.min..nums.max).to_a
  result = arr - nums
end

# Write a method that given a string representation of a binary number will
# return that binary number in base 10.
#
# To convert from binary to base 10, we take the sum of each digit multiplied by
# two raised to the power of its index. For example:
#   1001 = [ 1 * 2^3 ] + [ 0 * 2^2 ] + [ 0 * 2^1 ] + [ 1 * 2^0 ] = 9
#
# You may NOT use the Ruby String class's built in base conversion method.

def base2to10(binary)
  how_long = (binary.length - 1)
  result = 0
  binary.to_s.chars.each_with_index do |n, i|
    result += n.to_i * 2 ** (how_long - i)
  end
  result

end

class Hash

  # Hash#select passes each key-value pair of a hash to the block (the proc
  # accepts two arguments: a key and a value). Key-value pairs that return true
  # when passed to the block are added to a new hash. Key-value pairs that return
  # false are not. Hash#select then returns the new hash.
  #
  # Write your own Hash#select method by monkey patching the Hash class. Your
  # Hash#my_select method should have the functionailty of Hash#select described
  # above. Do not use Hash#select in your method.

  def my_select(&prc)
    result = Hash.new
    self.each do |k, v|
      result[k] = v if prc.call(k, v)
    end
    result
  end

end

class Hash

  # Hash#merge takes a proc that accepts three arguments: a key and the two
  # corresponding values in the hashes being merged. Hash#merge then sets that
  # key to the return value of the proc in a new hash. If no proc is given,
  # Hash#merge simply merges the two hashes.
  #
  # Write a method with the functionality of Hash#merge. Your Hash#my_merge method
  # should optionally take a proc as an argument and return a new hash. If a proc
  # is not given, your method should provide default merging behavior. Do not use
  # Hash#merge in your method.

  def my_merge(hash, &prc)
    result = Hash.new
    # prc ||= Proc.new { |key, oldval, newval| newval }
    if prc.nil?
      self.each do |k, v|
        result[k] = v
      end
      hash.each do |k, v|
        result[k] = v
      end
      return result
    end

    self.each do |k, v|
      next if hash[k].nil?
      result[k] = prc.call(k, v, hash[k])
    end
    hash.each do |k, v|
      result[k] = v unless result.has_key?(k)
    end
    result
  end

end

# The Lucas series is a sequence of integers that extends infinitely in both
# positive and negative directions.
#
# The first two numbers in the Lucas series are 2 and 1. A Lucas number can
# be calculated as the sum of the previous two numbers in the sequence.
# A Lucas number can also be calculated as the difference between the next
# two numbers in the sequence.
#
# All numbers in the Lucas series are indexed. The number 2 is
# located at index 0. The number 1 is located at index 1, and the number -1 is
# located at index -1. You might find the chart below helpful:
#
# Lucas series: ...-11,  7,  -4,  3,  -1,  2,  1,  3,  4,  7,  11...
# Indices:      ... -5, -4,  -3, -2,  -1,  0,  1,  2,  3,  4,  5...
#
# Write a method that takes an input N and returns the number at the Nth index
# position of the Lucas series.

def lucas_numbers(n)

  if n < 0
    lucas = Array.new(n.abs + 1)
    lucas[0] = 2
    lucas[1] = -1
    lucas.map.with_index do |e, i|
      if i > 1
        lucas[i] = lucas[i - 2] - lucas[i - 1]
      end
    end
    return lucas[n.abs]
  end

  if n >= 0
    lucas = Array.new(n + 1)
    lucas[0] = 2
    lucas[1] = 1
    lucas.map.with_index do |e, i|
      if i > 1
        lucas[i] = lucas[i - 1] + lucas[i - 2]
      end
    end
    return lucas[n]
  end

end


# A palindrome is a word or sequence of words that reads the same backwards as
# forwards. Write a method that returns the [CORRECTION: LENGTH OF THE] longest
# palindrome in a given string.
# If there is no palindrome longer than two letters, return false.

def longest_palindrome(string)
  rev_str = string.reverse
  palindromes = []
  (0...string.length).each do |i|
    (i...string.length).each do |j|
      if rev_str.include? string[i..j]
        palindromes << string[i..j]
      end
    end
  end

  palindromes.sort_by! { |n| n.length }
  return false if palindromes[-1].length < 3
  palindromes[-1].length
end

def longest_palindrome_in_sentence(sentence)
  arr = sentence.split(" ")
  pals = arr.select {|e| e == e.reverse }
  result = pals.sort_by {|e| e.length } #check2 = pals.select {|e| e.length > 2 }
  return false if result[-1].nil? || result[-1].length < 3
  result[-1]
end
